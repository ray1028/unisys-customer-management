import axios from "axios";

class CustomerService {
  static fetchAllCustomers = () => {
    return axios
      .get("https://jsonplaceholder.typicode.com/users")
      .then(resp => resp.data)
      .catch(err => console.log(err));
  };
}

export default CustomerService;

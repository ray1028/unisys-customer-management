import React from "react";
import { Provider } from "react-redux";
import configureStore from "./redux/store";
import "./App.css";
import { BrowserRouter as Router } from "react-router-dom";
import Routes from "./Routes";

const { store } = configureStore();

function App() {
  return (
    <Provider store={store}>
      <Router>
        <Routes />
      </Router>
    </Provider>
  );
}

export default App;

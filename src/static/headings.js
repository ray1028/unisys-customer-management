const headings = {
  ID: "ID",
  Name: "Name",
  Email: "Email",
  Phone: "Phone",
  Street: "Street",
  City: "City",
  Company: "Company",
  Action: "Action"
};

export default headings;

import React, { Component } from "react";
import Button from "../button/Button";
import PropTypes from "prop-types";
import "./Header.css";

class Header extends Component {
  render() {
    return (
      <nav>
        <ul>
          <li>
            <h2>Manage Customer</h2>
          </li>
          {this.props.disp && (
            <li>
              <Button
                buttonType="add"
                onClickHandler={this.props.onNewCustomerHandler}
              >
                New Customer
              </Button>
            </li>
          )}
        </ul>
      </nav>
    );
  }
}

Header.propTypes = {
  onNewCustomerHandler: PropTypes.func
};

export default Header;

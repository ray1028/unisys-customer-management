import React, { Component } from "react";
import PropTypes from "prop-types";
import Button from "../button/Button";
import "./TableRow.css";

class TableRow extends Component {
  handleDelete = id => {
    const result = window.confirm(`Do you want to delete customer ID ${id}`);
    if (result) {
      this.props.onDeleteHandler(this.props.customer.id);
    }
  };

  render() {
    return (
      <tr>
        <td>{this.props.customer.id}</td>
        <td>{this.props.customer.name}</td>
        <td>{this.props.customer.email}</td>
        <td>{this.props.customer.phone}</td>
        <td>{this.props.customer.address.street}</td>
        <td>{this.props.customer.address.city}</td>
        <td>{this.props.customer.company.name}</td>
        <td>
          <Button
            buttonType="edit"
            onClickHandler={() =>
              this.props.onEditHandler(this.props.customer.id)
            }
          >
            Edit
          </Button>
          <Button
            buttonType="delete"
            onClickHandler={() => {
              this.handleDelete(this.props.customer.id);
            }}
          >
            Delete
          </Button>
        </td>
      </tr>
    );
  }
}

TableRow.propTypes = {
  customer: PropTypes.shape({
    id: PropTypes.number,
    name: PropTypes.string,
    email: PropTypes.string,
    phone: PropTypes.string,
    street: PropTypes.string,
    city: PropTypes.string,
    company: PropTypes.object
  }),
  onDeleteHandler: PropTypes.func,
  onEditHandler: PropTypes.func
};

export default TableRow;

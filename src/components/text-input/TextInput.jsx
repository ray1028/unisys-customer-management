import React, { Fragment } from "react";
import "./TextInput.css";

const TextInput = props => {
  return (
    <Fragment>
      <p className="field-label">{props.label}</p>
      <input
        disabled={props.name === "id" ? true : false}
        type={props.type}
        name={props.name}
        defaultValue={props.value}
        onChange={props.handleChange}
        onKeyDown={e => (e.keyCode === 13 ? e.preventDefault() : "")}
      />
    </Fragment>
  );
};

export default TextInput;

import React, { Component } from "react";
import PropTypes from "prop-types";
import "./TableHeader.css";

class TableHeader extends Component {
  tableHeaders = this.props.headings.map(heading => (
    <th key={heading}>{heading}</th>
  ));
  render() {
    return (
      <thead>
        <tr>{this.tableHeaders}</tr>
      </thead>
    );
  }
}

TableHeader.propTypes = {
  headings: PropTypes.array
};

export default TableHeader;

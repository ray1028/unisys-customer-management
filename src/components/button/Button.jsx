import React, { Component } from "react";
import "./Button.css";
import PropTypes from "prop-types";

const classNames = require("classnames");

class Button extends Component {
  render() {
    const buttonClass = classNames("button", {
      "button-edit": this.props.buttonType === "edit",
      "button-delete": this.props.buttonType === "delete",
      "button-back":
        this.props.buttonType === "back" || this.props.buttonType === "add"
    });
    return (
      <button className={buttonClass} onClick={this.props.onClickHandler}>
        {this.props.children}
      </button>
    );
  }
}

Button.propTypes = {
  onClickHandler: PropTypes.func,
  buttonType: PropTypes.string
};

export default Button;

import React, { Component } from "react";
import "./Detail.css";
import { connect } from "react-redux";
import {
  getCustomerById,
  updateCustomer,
  addNewCustomer
} from "../../../redux/actions/customerAction";
import Button from "../../button/Button";
import TextInput from "../../text-input/TextInput";
import Header from "../../header/Header";

// 1.	Display id, name, email, address, phone, website and company information
class Detail extends Component {
  isEdit = false;

  constructor(props) {
    super(props);
    this.state = {
      user: {
        id: "",
        company: "",
        catchPhrase: "",
        business: "",
        name: "",
        username: "",
        email: "",
        phone: "",
        website: "",
        street: "",
        suite: "",
        city: "",
        zipcode: ""
      }
    };
  }

  componentDidMount() {
    let id;
    if (this.props.match.params.id) {
      this.isEdit = true;
      id = this.props.match.params.id;
      const currentCus = this.props.customers.find(c => c.id === +id);
      if (currentCus) {
        this.setState({
          user: {
            id: currentCus.id,
            company: currentCus.company.name,
            catchPhrase: currentCus.company.catchPhrase,
            business: currentCus.company.bs,
            name: currentCus.name,
            username: currentCus.username,
            email: currentCus.email,
            phone: currentCus.phone,
            website: currentCus.website,
            street: currentCus.address.street,
            suite: currentCus.address.suite,
            city: currentCus.address.city,
            zipcode: currentCus.address.zipcode
          }
        });
      }
    } else {
      this.isEdit = false;
      const lastCustomer = this.props.customers[
        this.props.customers.length - 1
      ];
      this.setState({
        user: {
          id: lastCustomer ? lastCustomer.id + 1 : 1
        }
      });
    }
  }

  onChangeHandler = event => {
    const { name, value } = event.target;
    if (event.target.name) {
      this.setState(prevState => ({
        user: { ...prevState.user, [name]: value }
      }));
    }
  };

  mapCustomerObject = user => {
    const { id } = this.props.match.params;
    let currentCus;
    if (this.isEdit) {
      currentCus = this.props.customers.find(c => c.id === +id);
    }
    return {
      id: this.isEdit ? currentCus.id : user.id,
      name: user.name,
      username: user.username,
      email: user.email,
      phone: user.phone,
      address: {
        street: user.street,
        suite: user.suite,
        city: user.city,
        zipcode: user.zipcode,
        geo: currentCus ? currentCus.geo : {},
        website: user.website
      },
      company: {
        name: user.company,
        catchPhrase: user.catchPhrase,
        bs: user.business
      }
    };
  };

  submitHandler = event => {
    event.preventDefault();
    const { user } = this.state;
    if (this.props.match.params.id) {
      this.props.updateCustomer(this.mapCustomerObject(user));
    } else {
      this.props.addNewCustomer(this.mapCustomerObject(user));
    }
    this.props.history.push("/");
  };

  goPrevious = event => {
    event.preventDefault();
    this.props.history.goBack();
  };

  render() {
    return (
      <div className="detail-page-container">
        <Header onNewCustomerHandler={() => this.onAddHandler()}></Header>
        <form
          onSubmit={e => {
            e.preventDefault();
          }}
        >
          <div className="detail-container">
            <div className="card-grid">
              <div className="item-title">
                <p className="card-grid__title">Customer Information</p>
                <hr></hr>
              </div>
              <div className="item-id">
                <TextInput
                  type={"text"}
                  name={"id"}
                  label={"Id"}
                  value={this.state.user.id}
                  handleChange={e => this.onChangeHandler(e)}
                  isRequired
                />
              </div>

              <div className="item-company">
                <TextInput
                  type={"text"}
                  name={"company"}
                  label={"Company Name"}
                  value={this.state.user.company}
                  handleChange={e => this.onChangeHandler(e)}
                  isRequired
                />
              </div>
              <div className="item-cp">
                <TextInput
                  type={"text"}
                  name={"catchPhrase"}
                  label={"Catch Phrase"}
                  value={this.state.user.catchPhrase}
                  handleChange={e => this.onChangeHandler(e)}
                  isRequired
                />
              </div>

              <div className="item-business">
                <TextInput
                  type={"text"}
                  name={"business"}
                  label={"Business"}
                  value={this.state.user.business}
                  handleChange={e => this.onChangeHandler(e)}
                  isRequired
                />
              </div>
              <div className="item-name">
                <TextInput
                  type={"text"}
                  name={"name"}
                  label={"Name"}
                  value={this.state.user.name}
                  handleChange={e => this.onChangeHandler(e)}
                  isRequired
                />
              </div>
              <div className="item-username">
                <TextInput
                  type={"text"}
                  name={"username"}
                  label={"Username"}
                  value={this.state.user.username}
                  handleChange={e => this.onChangeHandler(e)}
                  isRequired
                />
              </div>
              <div className="item-email">
                <TextInput
                  type={"email"}
                  name={"email"}
                  label={"Email"}
                  value={this.state.user.email}
                  handleChange={e => this.onChangeHandler(e)}
                  isRequired
                />
              </div>
              <div className="item-phone">
                <TextInput
                  type={"text"}
                  name={"phone"}
                  label={"Phone"}
                  value={this.state.user.phone}
                  handleChange={e => this.onChangeHandler(e)}
                  isRequired
                />
              </div>
              <div className="item-website">
                <TextInput
                  type={"text"}
                  name={"website"}
                  label={"Website"}
                  value={this.state.user.website}
                  handleChange={e => this.onChangeHandler(e)}
                  isRequired
                />
              </div>
              <div className="item-street">
                <TextInput
                  type={"text"}
                  name={"street"}
                  label={"Street Address"}
                  value={this.state.user.street}
                  handleChange={e => this.onChangeHandler(e)}
                  isRequired
                />
              </div>
              <div className="item-suite">
                <TextInput
                  type={"text"}
                  name={"suite"}
                  label={"Suite#"}
                  value={this.state.user.suite}
                  handleChange={e => this.onChangeHandler(e)}
                  isRequired
                />
              </div>
              <div className="item-city">
                <TextInput
                  type={"text"}
                  name={"city"}
                  label={"City"}
                  value={this.state.user.city}
                  handleChange={e => this.onChangeHandler(e)}
                  isRequired
                />
              </div>
              <div className="item-zipcode">
                <TextInput
                  type={"text"}
                  name={"zipcode"}
                  label={"Zipcode"}
                  value={this.state.user.zipcode}
                  handleChange={e => this.onChangeHandler(e)}
                  isRequired
                />
              </div>

              <div className="item-back-button">
                <Button
                  buttonType="back"
                  onClickHandler={e => this.goPrevious(e)}
                >
                  back
                </Button>
              </div>
              <div className="item-save-button">
                <Button
                  buttonType="edit"
                  onClickHandler={e => this.submitHandler(e)}
                >
                  Save
                </Button>
              </div>
            </div>
          </div>
        </form>
      </div>
    );
  }
}
const mapDispatchToProps = {
  getCustomerById,
  updateCustomer,
  addNewCustomer
};

const mapStateToProps = state => {
  return {
    customers: state.customerReducer.customers,
    currentCustomer: state.customerReducer.currentCustomer
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Detail);

import React, { Component } from "react";
import Table from "../../table/Table";
import "./Main.css";
import staticHeadings from "../../../static/headings";
import Header from "../../header/Header";
import history from "../../../history";

class Main extends Component {
  headings = Object.keys(staticHeadings);

  onAddHandler = () => {
    history.push("/customers/new");
  };
  onEditHandler = id => {
    history.push(`/customers/${id}`);
  };

  render() {
    return (
      <div className="table-container">
        <Header
          onNewCustomerHandler={() => this.onAddHandler()}
          disp={true}
        ></Header>
        <Table headings={this.headings} onEditHandler={this.onEditHandler} />
      </div>
    );
  }
}

export default Main;

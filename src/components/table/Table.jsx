import React, { Component } from "react";
import TableHeader from "../table-header/TableHeader";
import TableRow from "../table-row/TableRow";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { setCustomers } from "../../redux/actions/customerAction";
import { deleteCustomer } from "../../redux/actions/customerAction";
import "./Table.css";

class Table extends Component {
  componentDidMount() {
    if (this.props.customers.length === 0) {
      this.props.setCustomers();
    }
  }

  render() {
    return (
      <div className="customer-table-container">
        <table className="customer-table">
          <TableHeader headings={this.props.headings}></TableHeader>
          <tbody>
            {this.props.customers.map(customer => {
              return (
                <TableRow
                  key={customer.id}
                  customer={customer}
                  onDeleteHandler={this.props.deleteCustomer}
                  onEditHandler={this.props.onEditHandler}
                ></TableRow>
              );
            })}
          </tbody>
        </table>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    customers: state.customerReducer.customers
  };
};

const mapDispatchToProps = {
  setCustomers,
  deleteCustomer
};

Table.propTypes = {
  headings: PropTypes.array,
  deleteCustomer: PropTypes.func,
  onEditHandler: PropTypes.func
};

export default connect(mapStateToProps, mapDispatchToProps)(Table);

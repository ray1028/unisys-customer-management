import actionTypes from "../action-types/actionTypes";
import CustomerService from "../../services/customerService";

export const setCustomers = () => {
  return async dispatch => {
    const customers = await CustomerService.fetchAllCustomers();
    if (customers.length > 0) {
      return dispatch({
        type: actionTypes.SET_CUSTOMERS,
        payload: customers
      });
    }
  };
};

export const deleteCustomer = id => {
  return dispatch => {
    return dispatch({
      type: actionTypes.DELETE_CUSTOMERS,
      payload: id
    });
  };
};

export const getCustomerById = id => {
  return dispatch => {
    return dispatch({
      type: actionTypes.GET_CUSTOMER_BY_ID,
      payload: id
    });
  };
};

export const updateCustomer = updatedCus => {
  return dispatch => {
    return dispatch({
      type: actionTypes.UPDATE_CUSTOMERS,
      payload: updatedCus
    });
  };
};

export const addNewCustomer = newCus => {
  return dispatch => {
    return dispatch({
      type: actionTypes.ADD_CUSTOMER,
      payload: newCus
    });
  };
};

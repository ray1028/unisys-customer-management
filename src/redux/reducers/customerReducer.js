import actionTypes from "../action-types/actionTypes";

const initialState = {
  customers: [],
  currentCustomer: {}
};

export default (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.SET_CUSTOMERS:
      return { ...state, customers: action.payload };
    case actionTypes.DELETE_CUSTOMERS:
      return {
        ...state,
        customers: state.customers.filter(c => c.id !== action.payload)
      };
    case actionTypes.UPDATE_CUSTOMERS:
      const newCustomers = state.customers.map(cus =>
        cus.id === action.payload.id ? action.payload : cus
      );
      return {
        ...state,
        customers: newCustomers
      };
    case actionTypes.ADD_CUSTOMER:
      return { ...state, customers: [...state.customers, action.payload] };
    default:
      return state;
  }
};

import { combineReducers } from "redux";
import customerReducer from "../reducers/customerReducer";
export default combineReducers({
  customerReducer
});

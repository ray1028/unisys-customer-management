import React, { Component } from "react";
import { Router, Switch, Route } from "react-router-dom";
import history from "./history";
import Main from "./components/pages/main/Main";
import Detail from "./components/pages/detail/Detail";

class Routes extends Component {
  render() {
    return (
      <Router history={history}>
        <Switch>
          <Route path="/" exact component={Main} />
          <Route exact path="/customers/new" component={Detail} />
          <Route exact path="/customers/:id" component={Detail} />
        </Switch>
      </Router>
    );
  }
}

export default Routes;
